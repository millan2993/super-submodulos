import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ButtonsSmAComponent } from './components/submodule-a/buttons-sm-a.component';

@NgModule({
  declarations: [
    AppComponent,
    ButtonsSmAComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
